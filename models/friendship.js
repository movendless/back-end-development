var mongoose = require('mongoose'),
    ObjectId = mongoose.Schema.ObjectId;

var friendShip = mongoose.Schema({
    createdAt: { type: Date, default: Date.now },
    friend1: {
        id: { type: ObjectId },
    },
    friend2: {
        id: { type: ObjectId }
    },
    deleted: { type: Boolean, default: false }
});

module.exports = mongoose.model('friendShip', friendShip);