var mongoose = require('mongoose'),
    ObjectId = mongoose.Schema.ObjectId;

// define the schema for our user model
var friendRequest = mongoose.Schema({
    createdAt: { type: Date, default: Date.now },
    sender: {
        id: { type: ObjectId },
    },
    sendTo: {
        id: { type: ObjectId }
    },
    approved: { type: Boolean, default: false }
});

// create the model for users and expose it to our app
module.exports = mongoose.model('friendRequest', friendRequest);