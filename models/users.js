// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({
    local : {
        picture : { type: String, default: "https://cdn0.vox-cdn.com/images/verge/default-avatar.v9899025.gif" },
        nameSurname : { type: String, trim: true },
        username : { type: String, trim: true, unique: true },
        email : { type: String, trim: true, unique: true },
        password : String,
        emailValid : { type: Boolean, default: false },
        valid : { type: Boolean, default: false },
    }
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync());
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    console.log('users.js - password:');
    console.log(password);
    console.log('users.js - this.local.password:');
    console.log(this.local.password);
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);