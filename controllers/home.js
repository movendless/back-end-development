module.exports = function(app, passport) {

    app.get('/', isLoggedIn, function(req, res) {
        res.render('home', {
        	user: req.user
        });
    });

}

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.render('login-register');
}