var User = require('../models/users');
var async = require('async');
var friendRequest = require('../models/friend-request');
var friendship = require('../models/friendship');

module.exports = function(app, passport) {


	// Friend Requests Page
	app.get('/friend-requests', isLoggedIn, function(req, res) {

		friendRequest.find({ 'sendTo.id' : req.user._id }, function(err, reqFind) {
		    if (reqFind) {
		    	async.each(reqFind, function(request, callback){

			        User.findOne({'_id': request.sender.id}, function (err, findedUser) {
			        	if (!err){
			        		request.name = findedUser.local.nameSurname;
			        		callback();
			        	} else {
			        		callback(err);
			        	}
			        });

		    	}, function(err){
		    		if (!err){
		    			res.render('friend-requests', {
				        	requests: reqFind
				        });
	    			} else {
	    				// Use this: throw err
	    			}
		    	});

		    }
		});
    });

	app.get('/friend-requests/add/:id', isLoggedIn, function(req, res) {
		console.log('Friend-request.js - Accept : ' + req.params.id);

		User.findOne({ '_id' : req.params.id }, function(err, approveFind) {
		    if (approveFind) {
		        console.log(approveFind._id);

		        // set the user's local credentials
				var newFriend = new friendship();
		        newFriend.friend1.id = req.user._id;
		        newFriend.friend2.id = approveFind._id;

		        // save the user
		        newFriend.save(function(err) {
		            if (err) {
		                console.log(err); // Use this: flash error
	                } else {
						res.redirect('/friend-requests');
	                }
		        });
		    }
		});
    });

    // Accept Friend Request

	// Send friend request
	app.get('/friend-request/:username', function(req, res) {
		console.log(req.params.username);

		User.findOne({ 'local.username' : req.params.username }, function(err, userFind) {
		    if (userFind) {
		        console.log(userFind._id);

		        // set the user's local credentials
				var newFriendRequest = new friendRequest();
		        newFriendRequest.sendTo.id = userFind._id;
		        newFriendRequest.sender.id = req.user._id;

		        // save the user
		        newFriendRequest.save(function(err) {
		            if (err) {
	                	throw err;
		            } else {
        				res.redirect('/'); // Use this: flash for approved message
		            }
		        });
		    }
		});

    });

}

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}