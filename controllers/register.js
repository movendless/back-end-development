module.exports = function(app, passport) {

	app.get('/register', function(req, res) {
        res.render('register', {
        	message: req.flash('signupMessage')	
        });
    });

	app.post('/register', passport.authenticate('local-signup', {
	    successRedirect : '/', // redirect to the secure profile section
	    failureRedirect : '/register', // redirect back to the signup page if there is an error
	    failureFlash : true // allow flash messages
	}));

}