var User = require('../models/users');
var bcrypt   = require('bcrypt-nodejs');

module.exports = function(app, passport) {

    app.get('/settings', isLoggedIn, function(req, res) {
        res.render('settings', {
            user: req.user, // user variable
            itself: true, // Self page
            loginned: true // Loginned
        });
    });

    app.post('/settings', isLoggedIn, function (req, res) {

        var updatedName = req.user.local.nameSurname,
            updatedEmail = req.user.local.email,
            updatedPassword;

        // Name Surname Control
        if (!isNullOrWhiteSpace(req.body.nameSurname)) {
            updatedName = req.body.nameSurname;
        }

        // Password Control
        if (req.body.password[0].length > 4 && req.body.password[0] === req.body.password[1] && !isNullOrWhiteSpace(req.body.nameSurname)) {
            updatedPassword = bcrypt.hashSync(req.body.password[0], bcrypt.genSaltSync());
        } else {
            updatedPassword = req.user.local.password;
        }

        // Email control
        if (!isNullOrWhiteSpace(req.body.email)) {
            updatedEmail = req.body.email;
        }

        User.findByIdAndUpdate({'_id': req.user._id},
            {
                'local.nameSurname': updatedName,
                'local.email': updatedEmail,
                'local.password': updatedPassword,
            }, function (err, data) {
                if (err) {
                    console.log(err);
                }

                if(data) {
                    res.redirect('/settings');
                }
        })
    });
}

function isLoggedIn(req, res, next) {

    // if user is authenticated in the session
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

// Space control
function isNullOrWhiteSpace(str) {
  return (!str || str.length === 0 || /^\s*$/.test(str))
}