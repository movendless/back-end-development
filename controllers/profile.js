var User = require('../models/users');

module.exports = function(app, passport) {

    app.get('/profile/:username', function(req, res) {

        if (req.isAuthenticated()) {
            // If logged in
            if (req.params.username == req.user.local.username) {
                // If itself profile
                res.render('profile', {
                    user: req.user, // user variable
                    itself: true, // Self page
                    loginned: true, // Loginned
                });
            } else {
                // Else itself profile
                User.findOne({ 'local.username' : req.params.username }, function(err, userFind) {
                    if (userFind) {
                        res.render('profile', {
                            user: userFind,
                            loginned: true, // Loginned
                            itself: false // Self Page
                        });
                    } else {
                        res.redirect('/'); // render 404 page
                        console.log(err);
                    }
                });
            }
        } else {
            // Not loginned
            User.findOne({ 'local.username' : req.params.username }, function(err, userFind) {
                if (userFind) {
                    // User finded
                    res.render('profile', {
                        user: userFind,
                        logged: false, // Loginned
                        itself: false // Self Page
                    });
                } else {
                    // User not finded
                    res.redirect('/'); // render 404 page
                    console.log(err);
                }
            });
        }
    });

	app.get('/profile', isLoggedIn, function(req, res) {
        res.redirect('/profile/' + req.user.local.username);
    });

}

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}