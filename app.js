/*=================================
=            Variables            =
=================================*/

var bodyParser     = require('body-parser'),
	cookieParser   = require('cookie-parser'),
	express        = require('express'),
	exphbs         = require('express-handlebars'),
	path           = require('path'),
	serveStatic    = require("serve-static"),
	app            = express(),
	config         = require('./configs'),
	helmet         = require('helmet'),
	router         = express.Router(),
	mongoose        = require('mongoose'),
	session        = require('express-session'),
	passport       = require('passport'),
	flash          = require('connect-flash'),
    hbs;

/*=====  End of Variables  ======*/

/*===============================
=            Configs            =
===============================*/
mongoose.connect(config.db.username + ':' + config.db.password + '@' + config.db.ip + '/' + config.db.name + '?authMechanism=SCRAM-SHA-1');
var db = mongoose.connection;

db.once('open', function() {
  // we're connected!
  console.log('Db Connected')
});

app.use(cookieParser());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(helmet());
app.use(serveStatic(__dirname + '/public'));

// Router Config
router.use(function(req, res, next) {
    next();
});

app.use('/', require('./controllers'));

// View Engine Configs
hbs = exphbs.create({
    extname: '.hbs',
    defaultLayout: 'layout'
});
app.set('views', path.join(__dirname, 'views'));
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');

// required for passport
app.use(session({ secret: 'movendlesssecretprokey' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
require('./config/passport')(passport);

// Route files
require('./controllers/login')(app, passport);
require('./controllers/register')(app, passport);
require('./controllers/logout')(app, passport);
require('./controllers/home')(app, passport);
require('./controllers/profile')(app, passport);
require('./controllers/friend-request')(app, passport);
require('./controllers/settings')(app, passport);

/*=====  End of Configs  ======*/

app.listen(config.site.port, function () {
	console.log('Site working on ' + config.site.port + ' port');
});